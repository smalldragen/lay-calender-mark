# layCalenderMark

#### 背景
参考过其他的插件，不是很满意，然后综合了一下，自己重新整合了一下做成了demo，暂未做成插件。不是很完美，有问题多多指教

#### 介绍
基于laydate、contextMenu的日历右键标注功能

#### 预览地址
https://stackblitz.com/edit/stackblitz-starters-ifyirihs?file=index.html

#### 相关插件
laydate + contextMenu.js

#### 使用说明
参考示例即可，简单易上手

#### 效果截图

![输入图片说明](https://foruda.gitee.com/images/1694511014650740412/70ba4715_10166968.png "calendar.png")
